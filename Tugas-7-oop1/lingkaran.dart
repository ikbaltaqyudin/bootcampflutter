class Lingkaran {
  double? _jarijari;
  double? _phi;

  double get jarijari {
    return _jarijari!;
  }

  void set jarijari(double jari) {
    if (jari < 0) {
      jari *= -1;
    }
    _jarijari = jari;
  }

  double get phi {
    return _phi!;
  }

  void set phi(double value) {
    if (value < 0) {
      value *= -1;
    }
    _phi = value;
  }

  double get luasLingkaran => _jarijari! * _jarijari! * _phi!;
}
