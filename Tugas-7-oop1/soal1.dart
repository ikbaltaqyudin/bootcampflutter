main(List<String> args) {
  hitungLuasSegitiga segitiga;
  double? luasSegitiga;

  segitiga = new hitungLuasSegitiga();
  segitiga.alas = 20.0;
  segitiga.setengah = 0.5;
  segitiga.tinggi = 30.0;

  luasSegitiga = segitiga.luas();

  print(luasSegitiga);
}

class hitungLuasSegitiga {
  double? setengah;
  double? alas;
  double? tinggi;

  double luas() {
    return this.setengah! * alas! * tinggi!;
  }
}
