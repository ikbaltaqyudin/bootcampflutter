import 'lingkaran.dart';

main(List<String> args) {
  Lingkaran lingkaran;
  double? luasLingkaran;

  lingkaran = new Lingkaran();
  lingkaran.jarijari = 7.0;
  lingkaran.phi = 22 / 7;

  luasLingkaran = lingkaran.luasLingkaran;

  print(luasLingkaran);
}
