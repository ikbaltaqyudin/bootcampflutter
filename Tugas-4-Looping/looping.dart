import 'dart:io';

main(List<String> args) {
  //Tugas 1
  print("====================");
  print("TUGAS No.1");
  print("Looping Pertama");
  var i = 2;
  while (i <= 20) {
    print(" $i. I love coding");
    i += 2;
  }

  print("====================");
  print("Looping Kedua");
  var j = 20;
  while (j >= 1) {
    print(" $j. I will become a mobile developer");
    j = j - 2;
  }
  print("====================");

  // Tugas 2
  print("Tugas No.2");
  for (var i = 1; i <= 20; i++) {
    if (i % 2 == 0) {
      //genap
      print("$i. Berkualitas");
    } else if (i % 2 != 0 && i % 3 == 0) {
      //ganjil dan kelipatan 3
      print("$i. I Love Coding");
    } else {
      print("$i. Santai");
    }
  }
  print("====================");

  //Tugas 3
  print("Tugas No.3");
  for (var j = 1; j <= 4; j++) {
    for (var k = 1; k <= 8; k++) {
      stdout.write("#");
    }
    print("");
  }
  print("====================");

  //Tugas 4
  print("Tugas No.4");
  for (var j = 1; j <= 7; j++) {
    for (var k = 1; k <= j; k++) {
      stdout.write("#");
    }
    print("");
  }
  print("====================");
}
