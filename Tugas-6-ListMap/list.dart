main(List<String> args) {
  // Tugas 1
  print("====================");
  print("Tugas No. 1");
  print(range(1, 10));
  //Tugas 2
  print("====================");
  print("Tugas No. 2");
  print(rangeWithStep(10, 1, 3));
  //Tugas 3
  print("====================");
  print("Tugas No. 3");
  print("");

  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ];

  dataHandling(input);

  //Tugas 4
  print("====================");
  print("Tugas No. 4");
  print(balikKata("Kasur")); // rusaK
  print(balikKata("SanberCode")); // edoCrebnaS
  print(balikKata("Haji")); // ijaH
  print(balikKata("racecar")); // racecar
  print(balikKata("Sanbers")); // srebnaS
  print("====================");
}

range(int startnum, int finishnum) {
  List<int> bilangan = [];

  if (startnum < finishnum) {
    for (var index = startnum; index <= finishnum; index++) {
      bilangan.add(index);
    }
  } else {
    for (var index = startnum; index >= finishnum; index--) {
      bilangan.add(index);
    }
  }

  return bilangan;
}

rangeWithStep(int startnum, int finishnum, int step) {
  List<int> bilangan = [];

  if (startnum < finishnum) {
    for (var index = startnum; index <= finishnum; index += step) {
      bilangan.add(index);
    }
  } else {
    for (var index = startnum; index >= finishnum; index -= step) {
      bilangan.add(index);
    }
  }

  return bilangan;
}

dataHandling(List list) {
  var id = "";
  var nama = "";
  var tempat = "";
  var tanggal = "";
  var hobi = "";

  for (var i = 0; i <= list.length - 1; i++) {
    for (var j = 0; j <= 4; j++) {
      if (j == 0) {
        id = list[i][j];
      } else if (j == 1) {
        nama = list[i][j];
      } else if (j == 2) {
        tempat = list[i][j];
      } else if (j == 3) {
        tanggal = list[i][j];
      } else {
        hobi = list[i][j];
      }
    }
    print("Data Id : $id");
    print("Nama Lengkap : $nama ");
    print("TTL : $tempat $tanggal ");
    print("Hobi : $hobi");
    print("");
  }
}

balikKata(String kata) {
  // menggunakan looping (manual)
  var kataBaru = "";
  for (var index = kata.length - 1; index >= 0; index--) {
    kataBaru += kata[index];
  }
  return kataBaru;

  // menggunakan fungsi dari sdk (otomatis)
  // return kata.split("").reversed.join();
}
