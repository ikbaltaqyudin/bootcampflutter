import 'bangun_datar.dart';

class Persegi extends BangunDatar {
  double? s;

  Persegi(double s) {
    this.s = s;
  }

  @override
  double keliling() {
    return 4 * s!;
  }

  @override
  double luas() {
    return s! * s!;
  }
}
