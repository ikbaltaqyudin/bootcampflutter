import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  double? a;
  double? b;
  double? t;

  Segitiga(double a, double b, double t) {
    this.a = a;
    this.b = b;
    this.t = t;
  }

  double luas() {
    return 0.5 * a! * t!;
  }

  double keliling() {
    return a! + b! + t!;
  }
}
