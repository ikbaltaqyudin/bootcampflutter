import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  BangunDatar bangunDatar = new BangunDatar();
  Lingkaran lingkaran = new Lingkaran(3.14, 14);
  Persegi persegi = new Persegi(4);
  Segitiga segitiga = new Segitiga(3, 4, 5);

  bangunDatar.luas();
  bangunDatar.keliling();

  print(
      "Luas segitiga = ${segitiga.luas()} dan keliling = ${segitiga.keliling()}");
  print(
      "Luas lingkaran = ${lingkaran.luas()} dan keliling = ${lingkaran.keliling()}");
  print(
      "Luas persegi = ${persegi.luas()} dan keliling = ${persegi.keliling()}");
}
