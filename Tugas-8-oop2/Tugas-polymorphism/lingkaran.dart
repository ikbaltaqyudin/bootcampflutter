import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  double? phi;
  double? r;

  Lingkaran(double phi, double r) {
    this.phi = phi;
    this.r = r;
  }

  @override
  double luas() {
    return phi! * r! * r!;
  }

  @override
  double keliling() {
    return 2 * phi! * r!;
  }
}
