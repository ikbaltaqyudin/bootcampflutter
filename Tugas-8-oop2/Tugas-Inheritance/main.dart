import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  Armor armor = new Armor();
  Attack attack = new Attack();
  Beast beast = new Beast();
  Human human = new Human();

  armor.powerPoint = 3;
  attack.powerPoint = 7;
  beast.powerPoint = 9;
  human.powerPoint = 10;

  print("Powerpoint dari Armor Titan ${armor.powerPoint}");
  print("Powerpoint dari Attack Titan ${attack.powerPoint}");
  print("Powerpoint dari Beast Titan ${beast.powerPoint}");
  print("Powerpoint dari Humas ${human.powerPoint}");

  print("");

  print("Sifat dari Armor Titan " + armor.terjang());
  print("Sifat dari Attack Titan " + attack.punch());
  print("Sifat dari Beast Titan " + beast.lempar());
  print("Sifat dari Human " + human.killAllTitan());
}
