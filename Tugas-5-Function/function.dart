import 'dart:developer';

import 'dart:io';

main(List<String> args) {
  //Tugas No.1
  print("====================");
  print("Tugas No.1");
  print(teriak());

  // Tugas No.2
  print("====================");
  print("Tugas No.2");
  var num1 = 12;
  var num2 = 4;
  var hasilKali = kalikan(num1, num2);

  print(hasilKali);

  // Tugas No.3
  print("====================");
  print("Tugas No.3");
  var name = "Ikbal";
  var age = 24;
  var address = "Jln. Abadi regency no.1, Bandung";
  var hobby = "Badminton";

  var perkenalan = introduce(name, age, address, hobby);
  print(perkenalan);

  // Tugas 4
  print("====================");
  print("Tugas No.4");
  var bilangan = 6;

  print("Hasil faktorial adalah ");
  print(faktorial(bilangan));
  print("====================");
}

teriak() {
  return "Halo Sanbers!";
}

kalikan(int a, int b) {
  return a * b;
}

introduce(nama, umur, alamat, hobi) {
  return "Nama saya $nama, umur saya $umur tahun, alamat saya di $alamat,dan saya punya hobby yaitu $hobi!";
}

faktorial(int bil) {
  if (bil <= 0) {
    return 1;
  } else {
    // rekursif
    var num = bil * faktorial(bil - 1);
    return num;
  }
}
