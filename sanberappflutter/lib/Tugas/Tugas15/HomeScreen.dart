import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas12/DrawerScreen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      drawer: DrawerScreen(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 35.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                IconButton(
                  icon: Icon(Icons.add_shopping_cart),
                  onPressed: () {},
                ),
              ],
            ),
            SizedBox(height: 5),
            Text.rich(
              TextSpan(
                  text: 'Welcome,      ',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Color(0xff54C5F8)),
                  children: [
                    TextSpan(
                      text: ' Ikbal',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Color(0xff01579B)),
                    ),
                  ]),
              style: TextStyle(fontSize: 50),
            ),
            SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 18),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Color(0xff54C5F8))),
                hintText: 'Search',
              ),
            ),
            SizedBox(height: 20),
            Text(
              'Recomended Places',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
            ),
            SizedBox(height: 10),
            SizedBox(
              height: 200,
              child: GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.zero,
                childAspectRatio: 1.9,
                crossAxisSpacing: 2,
                mainAxisSpacing: 4,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  for (var country in countries)
                    Image.asset('assets/img/$country.png')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

final countries = ['Berlin', 'Monas', 'Roma', 'Tokyo'];
