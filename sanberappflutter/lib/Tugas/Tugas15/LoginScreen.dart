import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/DashboardScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/HomeScreen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 70),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text.rich(
                  TextSpan(
                    text: "Sanber Flutter",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Color(0xff54C5F8)),
                  ),
                  style: TextStyle(fontSize: 35),
                )
              ],
            ),
            SizedBox(height: 10),
            Image.asset('assets/img/flutter.png', width: 100),
            SizedBox(height: 20),
            TextField(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                hintText: 'Username',
              ),
            ),
            SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                enabledBorder:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                hintText: 'Password',
              ),
            ),
            SizedBox(height: 10),
            TextButton(
                onPressed: () {},
                child: Text('Forgot Password'),
                style: TextButton.styleFrom(primary: Color(0xff54C5F8))),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                  onPressed: () {
                    Route route = MaterialPageRoute(
                        builder: (context) => DashboardScreen());
                    Navigator.push(context, route);
                  },
                  child: Text('Login')),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Does not have account'),
                TextButton(
                    onPressed: () {},
                    child: Text('Sign up'),
                    style: TextButton.styleFrom(primary: Color(0xff54C5F8)))
              ],
            )
          ],
        ),
      ),
    );
  }
}
