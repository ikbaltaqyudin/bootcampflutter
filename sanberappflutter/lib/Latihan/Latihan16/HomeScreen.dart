import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sanberappflutter/Latihan/Latihan16/LoginScreen.dart';

class HomeScreen extends StatelessWidget {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                IconButton(
                  icon: Icon(Icons.add_shopping_cart),
                  onPressed: () {},
                ),
              ],
            ),
            SizedBox(height: 20),
            Text.rich(
              TextSpan(
                  text: 'Welcome,      ',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Color(0xff54C5F8)),
                  children: [
                    TextSpan(
                      text: auth.currentUser.email,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Color(0xff01579B)),
                    ),
                  ]),
              style: TextStyle(fontSize: 50),
            ),
            SizedBox(height: 30),
            TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 18),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Color(0xff54C5F8))),
                hintText: 'Search',
              ),
            ),
            SizedBox(height: 30),
            Text(
              'Recomended Places',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
            ),
            SizedBox(height: 10),
            // SizedBox(
            //   height: 300,
            //   child: GridView.count(
            //     crossAxisCount: 2,
            //     padding: EdgeInsets.zero,
            //     childAspectRatio: 1.8,
            //     crossAxisSpacing: 5,
            //     mainAxisSpacing: 5,
            //     physics: NeverScrollableScrollPhysics(),
            //     children: [
            //       for (var country in countries)
            //         Image.asset('assets/img/$country.png')
            //     ],
            //   ),
            // ),
            SizedBox(
                height: 30,
                child: ElevatedButton(
                  onPressed: () {
                    _signOut().then((value) => Navigator.of(context)
                        .pushReplacement(MaterialPageRoute(
                            builder: (context) => LoginScreen())));
                  },
                  child: Text("Logout"),
                ))
          ],
        ),
      ),
    );
  }
}

final countries = ['Berlin', 'Monas', 'Roma', 'Tokyo'];
