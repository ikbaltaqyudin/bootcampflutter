import 'package:flutter/material.dart';
import 'package:sanberappflutter/Latihan/Latihan17/Bloc/Bloc_Counter.dart';
import 'package:sanberappflutter/Latihan/Latihan17/Bloc/EventManager.dart';

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  final _bloc = Bloc_Counter();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Bloc State Management'),
      ),
      body: Center(
        child: StreamBuilder(
            stream: _bloc.counter,
            initialData: 0,
            builder: (BuildContext context, AsyncSnapshot<int> snip) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.network(
                      "https://raw.githubusercontent.com/felangel/bloc/master/docs/assets/bloc_logo_full.png",
                      width: 250,
                      height: 250),
                  Text('Press Bellow Button'),
                  Text("${snip.data}")
                ],
              );
            }),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.green,
            onPressed: () {
              print("Increment");
              _bloc.counterEventSink.add(IncrementEvent());
            },
          ),
          SizedBox(width: 10),
          FloatingActionButton(
            child: Icon(Icons.remove),
            backgroundColor: Colors.red,
            onPressed: () {
              print("Decrement");
              _bloc.counterEventSink.add(DecrementEvent());
            },
          ),
        ],
      ),
    );
  }
}
