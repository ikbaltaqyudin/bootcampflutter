main(List<String> args) {
  print("Life");
  delayedPrintText(3, "Never Flat!").then((status) {
    print(status);
  });
  print("is");
}

Future delayedPrintText(int sec, String mssg) {
  final duration = Duration(seconds: sec);
  return Future.delayed(duration).then((value) => mssg);
}
