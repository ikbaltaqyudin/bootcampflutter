void main(List<String> args) async {
  print("Ready sing ...");
  print(await line1());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line1() async {
  String lyrics = "pernahkan kau merasa";
  return Future.delayed(Duration(seconds: 5), () => (lyrics));
}

Future<String> line2() async {
  String lyrics = "pernahkan kau merasa.....";
  return Future.delayed(Duration(seconds: 3), () => (lyrics));
}

Future<String> line3() async {
  String lyrics = "pernahkan kau merasa";
  return Future.delayed(Duration(seconds: 2), () => (lyrics));
}

Future<String> line4() async {
  String lyrics = "Hatimu hampa, pernahkan kau merasa hati mu kosong....";
  return Future.delayed(Duration(seconds: 1), () => (lyrics));
}
