// import 'dart:async';

// void main(List<String> args) {
//   print("Starting");
//   var timer = Timer(Duration(seconds: -3), () => print("Timer Completed"));
//   print("Finished");
// }

// import 'dart:math';

// void main(List<String> args) {
//   var a = Point(2, 2);
//   var b = Point(4, 4);
//   var distance = Point.distanceBetween(a, b);

//   assert(2.8 < distance && distance < 2.9);
//   print(distance);
// }

// class Point {
//   double x, y;
//   Point(this.x, this.y);

//   static double distanceBetween(Point a, Point b) {
//     var dx = a.x - b.x;
//     var dy = a.y - b.y;
//     return sqrt(dx * dx + dy * dy);
//   }
// }

// Future<void> printOrderMessage() async {
//   print("Await user order");
//   var order = await fetchUserOrder();
//   print("Your order is : $order");
// }

// Future<String> fetchUserOrder() {
//   return Future.delayed(Duration(seconds: 4), () => 'Large Latte');
// }

// Future<void> main(List<String> args) async {
//   countSeconds(4);
//   await printOrderMessage();
// }

// void countSeconds(int s) {
//   for (var i = 1; i <= s; i++) {
//     Future.delayed(Duration(seconds: i), () => print(i));
//   }
// }



Future<void> printOrderMessage() async{
  try{
  var order = await fetchUserOrder();
  print("Await user order");
  print(order);
  } catch (err){
    print("Caught error : $err")
  }
}

Future<String> fetchUserOrder() {
  var str =  Future.delayed(Duration(seconds: 4), () => throw 'Cannot locate user order');
  return str;
}

Future<void> main(List<String> args) async {
  await printOrderMessage();
}