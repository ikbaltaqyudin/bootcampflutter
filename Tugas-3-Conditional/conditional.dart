import 'dart:io';

main(List<String> args) {
  //Tugas 1
  var isInstall = true;
  print("====================");
  print("TUGAS No.1");
  print("Apakah anda akan menginstall aplikasi : (Y/T)");
  String input = stdin.readLineSync()!;
  if (input == "Y" || input == "y") {
    isInstall = true;
  } else {
    isInstall = false;
  }
  isInstall ? print("anda akan menginstall aplikasi dart") : print("aborted");
  print("====================");

  //Tugas 2
  print("TUGAS No.2");
  print("Masukkan Nama = ");
  String inputNama = stdin.readLineSync()!;
  print("Masukkan Peran = ");
  String inputPeran = stdin.readLineSync()!;

  if (inputNama.isEmpty && inputPeran.isEmpty) {
    print("Nama harus diisi!");
  } else if (inputNama.isNotEmpty && inputPeran.isEmpty) {
    print("Hallo $inputNama , Pilih peranmu untuk memulai game!");
  } else if (inputNama.isNotEmpty && inputPeran.isNotEmpty) {
    var peran = inputPeran.toUpperCase();
    var job = "";

    if (peran == "PENYIHIR") {
      job = "kamu dapat melihat siapa yang menjadi werewolf!";
    } else if (peran == "GUARD") {
      job = "kamu akan membantu melindungi temanmu dari serangan werewolf!";
    } else if (peran == "WAREWOLF") {
      job = "kamu akan memakan mangsa setiap malam!";
    } else {
      job = "peran yang kamu pilih tidak ada dalam permainan ini";
    }

    print("Selamat datang di Dunia Werewolf, $inputNama !");
    print("Halo $peran $inputNama , $job");
  }
  print("====================");

  //Tugas 3
  print("TUGAS No.3");
  print("Masukkan Hari = ");
  String inputHari = stdin.readLineSync()!;
  var hari = inputHari.toUpperCase();
  switch (hari) {
    case "SENIN":
      {
        print(
            "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
      }
      break;
    case "SELASA":
      {
        print(
            "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
        break;
      }
    case "RABU":
      {
        print(
            "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
        break;
      }
    case "KAMIS":
      {
        print(
            "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
        break;
      }
    case "JUMAT":
      {
        print("Hidup tak selamanya tentang pacar.");
        break;
      }
    case "SABTU":
      {
        print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
        break;
      }
    case "MINGGU":
      {
        print(
            "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
        break;
      }
    default:
      {
        print("Cek inputan harimu kembali");
        break;
      }
  }

  print("====================");

  // Tugas No.4
  print("Tugas No.4");
  const tanggal =
      1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
  const bulan =
      1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
  const tahun =
      1745; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

  var namaBulan;
  switch (bulan) {
    case 1:
      {
        namaBulan = "Januari";
        break;
      }
    case 2:
      {
        namaBulan = "Februari";
        break;
      }
    case 3:
      {
        namaBulan = "Maret";
        break;
      }
    case 4:
      {
        namaBulan = "April";
        break;
      }
    case 5:
      {
        namaBulan = "Mei";
        break;
      }
    case 6:
      {
        namaBulan = "Juni";
        break;
      }
    case 7:
      {
        namaBulan = "Juli";
        break;
      }
    case 8:
      {
        namaBulan = "Agustus";
        break;
      }
    case 9:
      {
        namaBulan = "September";
        break;
      }
    case 10:
      {
        namaBulan = "Oktober";
        break;
      }
    case 11:
      {
        namaBulan = "November";
        break;
      }
    case 12:
      {
        namaBulan = "Desember";
        break;
      }
  }

  switch (true) {
    case (tanggal < 1 || tanggal > 31):
      print("Salah input tanggal");
      break;
    case (bulan < 1 || bulan > 12):
      print("Salah input bulan");
      break;
    case ((tahun < 1900 || tanggal > 2200)):
      print("Salah input tahun");
      break;
    default:
      switch (true) {
        case (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11):
          if (tanggal > 30) {
            print("Invalid bulan dan tanggal cek kembali inputan");
          } else {
            print("$tanggal $namaBulan $tahun");
          }
          break;
        case (bulan == 2):
          if (bulan % 4 != 0) {
            if (tanggal > 28) {
              print("Bukan tahun kabisat // cek kembali tanggal");
            } else {
              print("$tanggal $namaBulan $tahun");
            }
          }
          break;
        default:
          print("$tanggal $namaBulan $tahun");
          break;
      }
  }

  print("====================");
}
